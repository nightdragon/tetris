﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure_1 : Figure
{
	private void Awake ()
    {
        statement = new int[2, 2];
        statement[0, 0] = 0;
        statement[0, 1] = 1;
        statement[1, 0] = 2;
        statement[1, 1] = 3;
    }

    private void Start()
    {
        blocks = new List<IceBlock>();

        for(int i = 0; i < 4; i++)
            blocks.Add(GameController.GetFreeBlock());

        if (blocks.Count < 4)
        {
            Debug.Log("Something happened: pool have not enough blocks");
            return;
        }

        Appear();
    }

    protected override void Appear()
    {
        transform.position = GameController.field[GameController.field.GetLength(0) - 1, GameController.field.GetLength(1) / 2].coord + Vector3.up * 10;
        ud = GameController.field.GetLength(0) - 1;
        lr = GameController.field.GetLength(1) / 2;

        x = GameController.field[GameController.field.GetLength(0) - 1, GameController.field.GetLength(1) / 2].coord.x;
        y = GameController.field[GameController.field.GetLength(0) - 1, GameController.field.GetLength(1) / 2].coord.y;

        foreach (IceBlock b in blocks)
            b.transform.position = transform.position;

        ColorizeBlocks();

        ready = true;
    }

    protected override int Width()
    {
        return 2;
    }

    public override void TurnRight()
    {
        int temp = statement[0, 0];
        statement[0, 0] = statement[0, 1];
        statement[0, 1] = statement[1, 1];
        statement[1, 1] = statement[1, 0];
        statement[1, 0] = temp;
    }
    
    public override void TurnLeft()
    {
        int temp = statement[0, 0];
        statement[0, 0] = statement[1, 0];
        statement[1, 0] = statement[1, 1];
        statement[1, 1] = statement[0, 1];
        statement[0, 1] = temp;
    }
}
