﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplinterEmitter : MonoBehaviour
{
    private Vector3 startPos;
    private float time;
    private float start;

    private Rigidbody rb;

    private void Awake()
    {
        startPos = transform.localPosition;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Time.time - start < time)
            Melting();
        else
            gameObject.SetActive(false);
    }

    public void Explode(Vector3 center)
    {
        rb.AddForce((transform.position - center).normalized, ForceMode.Impulse);
    }

    public void SetVelocity(Vector3 v)
    {
        rb.velocity = v;
    }

    public void Melt(float t)
    {
        time = t;
        start = Time.time;
        transform.localPosition = startPos;
        transform.localScale = Vector3.one;
    }

    public void Melting()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, Time.deltaTime);
    }
}
