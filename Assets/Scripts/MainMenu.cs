﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject curtain;
    public Toggle[] gameMode;
    public Toggle skills;
    public InputField height;
    public InputField width;

    public void StartGame()
    {
        curtain.SetActive(true);

        for(int i = 0; i < gameMode.Length; i++)
            if(gameMode[i].isOn)
            {
                ChangeGameMode(i);
            }

        ChangeSkillsEnabled(skills.isOn);

        ChangeSize(int.Parse(width.text), int.Parse(height.text));

        SceneManager.LoadScene("game");
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void ChangeGameMode(GameMode gm)
    {
        GameProperties.gameMode = gm;
    }

    private void ChangeGameMode(int gm)
    {
        GameProperties.gameMode = (GameMode)gm;
    }

    private void ChangeSize(int width, int height)
    {
        GameProperties.width = width;
        GameProperties.height = height;
    }

    private void ChangeSkillsEnabled(bool en)
    {
        GameProperties.skills = en;
    }
}
