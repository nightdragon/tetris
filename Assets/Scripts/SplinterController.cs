﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplinterController : MonoBehaviour
{
    public float lifetime; //время "нормальной" что-ль жизни осколков
    
    private float startTimer; 
    private SplinterEmitter[] cells;

    private void Awake()
    {
        cells = GetComponentsInChildren<SplinterEmitter>();
    }

    private void Update()
    {
        if (Time.time - startTimer > lifetime)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Обработка появления
    /// </summary>
    public void Appear()
    {
        startTimer = Time.time; //запомним начальное время
        foreach (SplinterEmitter cell in cells)
        {
            cell.gameObject.SetActive(true);
            cell.Melt(lifetime / 2);
            cell.Explode(transform.position); //взорвём осколочки
        }
    }

    /// <summary>
    /// Задать начальную скорость осколков
    /// </summary>
    /// <param name="v">Вектор скорости</param>
    public void SetVelocity(Vector3 v)
    {
        foreach (SplinterEmitter cell in cells)
        {
            cell.SetVelocity(v);
        }
    }
}
