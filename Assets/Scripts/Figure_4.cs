﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure_4 : Figure
{
    bool rotated = false;

    private void Awake()
    {
        statement = new int[2, 3];
        statement[0, 0] = 0;
        statement[0, 1] = 1;
        statement[0, 2] = 2;
        statement[1, 0] = -1;
        statement[1, 1] = -1;
        statement[1, 2] = 3;
    }

    private void Start()
    {
        blocks = new List<IceBlock>();

        for (int i = 0; i < 4; i++)
            blocks.Add(GameController.GetFreeBlock());

        if (blocks.Count < 4)
        {
            Debug.Log("Something happened: pool have not enough blocks");
            return;
        }

        Appear();
    }

    protected override void Appear()
    {
        transform.position = GameController.field[GameController.field.GetLength(0) - 1, GameController.field.GetLength(1) / 2].coord + Vector3.up * 10;
        ud = GameController.field.GetLength(0) - 1;
        lr = GameController.field.GetLength(1) / 2;

        x = GameController.field[GameController.field.GetLength(0) - 1, GameController.field.GetLength(1) / 2].coord.x;
        y = GameController.field[GameController.field.GetLength(0) - 1, GameController.field.GetLength(1) / 2].coord.y;

        foreach (IceBlock b in blocks)
            b.transform.position = transform.position;

        ColorizeBlocks();

        ready = true;
    }

    protected override int Width()
    {
        if (rotated)
            return 2;
        else
            return 3;
    }

    public override void TurnRight()
    {
        rotated = !rotated;
        if (rotated)
        {
            int[,] newStatement = new int[3, 2];
            newStatement[0, 0] = statement[0, 2];
            newStatement[0, 1] = statement[1, 2];
            newStatement[1, 0] = statement[0, 1];
            newStatement[1, 1] = statement[1, 1];
            newStatement[2, 0] = statement[0, 0];
            newStatement[2, 1] = statement[1, 0];
            statement = newStatement;
        }
        else
        {
            int[,] newStatement = new int[2, 3];
            newStatement[0, 0] = statement[0, 1];
            newStatement[0, 1] = statement[1, 1];
            newStatement[0, 2] = statement[2, 1];
            newStatement[1, 0] = statement[0, 0];
            newStatement[1, 1] = statement[1, 0];
            newStatement[1, 2] = statement[2, 0];
            statement = newStatement;
        }
        if (lr + Width() - 1 >= GameController.field.GetLength(1))
            lr -= lr + Width() - GameController.field.GetLength(1);
    }

    public override void TurnLeft()
    {
        rotated = !rotated;
        if (rotated)
        {
            int[,] newStatement = new int[3, 2];
            newStatement[0, 0] = statement[1, 0];
            newStatement[0, 1] = statement[0, 0];
            newStatement[1, 0] = statement[1, 1];
            newStatement[1, 1] = statement[0, 1];
            newStatement[2, 0] = statement[1, 2];
            newStatement[2, 1] = statement[0, 2];
            statement = newStatement;
        }
        else
        {
            int[,] newStatement = new int[2, 3];
            newStatement[0, 0] = statement[2, 0];
            newStatement[0, 1] = statement[1, 0];
            newStatement[0, 2] = statement[0, 0];
            newStatement[1, 0] = statement[2, 1];
            newStatement[1, 1] = statement[1, 1];
            newStatement[1, 2] = statement[0, 1];
            statement = newStatement;
        }
        if (lr + Width() - 1 >= GameController.field.GetLength(1))
            lr -= lr + Width() - GameController.field.GetLength(1);
    }
}