﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Figure : MonoBehaviour
{
    public List<IceBlock> blocks;
    public float speed;
    public int[,] statement;
    public bool ready = false;

    protected int lr, ud;
    protected float x, y;

    private void Update()
    {
        if (!ready)
            return;

        x = Mathf.Lerp(x, GameController.field[0, lr].coord.x, Time.deltaTime * speed * 2);
        y = Mathf.MoveTowards(y, GameController.field[ud, 0].coord.y, Time.deltaTime * speed);
        transform.position = new Vector3(x, y, transform.position.z);
        CubeLocalReposition();

        if (y - GameController.field[ud, lr].coord.y <= 0.5f)
            Fall();
    }

    /// <summary>
    /// Сдвинуть фигуру влево
    /// </summary>
    public void MoveLeft()
    {
        if (lr > 0)
            if (CanMoveLeft())
                lr--;
    }

    /// <summary>
    /// Сдвинуть фигуру вправо
    /// </summary>
    public void MoveRight()
    {
        if (lr + Width() < GameController.field.GetLength(1))
            if (CanMoveRight())
                lr++;

    }

    /// <summary>
    /// Приземлить фигуру
    /// </summary>
    public void FallDown()
    {
        //Fall();
        while (ud > 0 && CanFall())
            ud--;

        Connect();

        StartCoroutine(Die());
    }

    /// <summary>
    /// Повернуть фигуру по часовой стрелке
    /// </summary>
    abstract public void TurnRight();

    /// <summary>
    /// Повернуть фигуру против часовой стрелки
    /// </summary>
    abstract public void TurnLeft();

    //сдвигаем кубы относительно фигуры
    private void CubeLocalReposition()
    {
        for (int i = 0; i < statement.GetLength(0); i++)
            for (int j = 0; j < statement.GetLength(1); j++)
            {
                if (statement[i, j] != -1)
                {
                    blocks[statement[i, j]].transform.position = Vector3.Lerp(blocks[statement[i, j]].transform.position,
                        new Vector3(transform.position.x + j * 2, transform.position.y + i * 2, transform.position.z),
                        Time.deltaTime * speed * 5);
                }
            }
    }

    //что делать при создании фигуры
    abstract protected void Appear();

    //вернём ширину фигуры
    abstract protected int Width();

    //приклеим фигуры к ячейкам на поле
    protected void Connect()
    {
        for (int i = 0; i < statement.GetLength(0); i++)
            for (int j = 0; j < statement.GetLength(1); j++)
            {
                if (statement[i, j] != -1)
                {
                    if (ud + i >= GameController.field.GetLength(0))
                    {
                        GameController.gameOver = true;
                        continue;
                    }
                    GameController.field[ud + i, lr + j].block = blocks[statement[i, j]];
                    blocks[statement[i, j]].PhysicsOn();
                }
            }
    }

    //раскрасим кубики
    protected void ColorizeBlocks()
    {
        Color c = new Color(Random.value, Random.value, Random.value, 0.25f);
        foreach (IceBlock b in blocks)
        {
            b.gameObject.GetComponent<Renderer>().material.color = c;
        }
    }

    //можно ли сдвинуть фигуру влево?
    private bool CanMoveLeft()
    {
        int[] possibility = new int[statement.GetLength(0)];

        for (int i = 0; i < statement.GetLength(0); i++)
            for (int j = 0; j < statement.GetLength(1); j++)
            {
                if (statement[i, j] != -1)
                {
                    possibility[i] = j;
                    break;
                }
            }

        for (int i = 0; i < statement.GetLength(0); i++)
            if (ud + i < GameController.field.GetLength(0))
                if (GameController.field[ud + i, lr + possibility[i] - 1].block != null)
                    return false;

        return true;
    }

    //можно ли сдвинуть фигуру вправо
    private bool CanMoveRight()
    {
        int[] possibility = new int[statement.GetLength(0)];

        for (int i = 0; i < statement.GetLength(0); i++)
            for (int j = statement.GetLength(1) - 1; j >= 0; j--)
            {
                if (statement[i, j] != -1)
                {
                    possibility[i] = j;
                    break;
                }
            }

        for (int i = 0; i < statement.GetLength(0); i++)
            if (ud + i < GameController.field.GetLength(0))
                if (GameController.field[ud + i, lr + possibility[i] + 1].block != null)
                    return false;

        return true;
    }

    //опустить фигуру на ячейку ниже
    private void Fall()
    {
        if (ud > 0 && CanFall())
            ud--;
        else
            Stop();
    }

    //остановить фигуру
    private void Stop()
    {
        if (GameController.current == this)
            GameController.current = null;
        Connect();

        StartCoroutine(Die());
    }

    //уничтожаем класс
    IEnumerator Die()
    {
        yield return new WaitForEndOfFrame();
        Destroy(this);
    }

    //можем ли сдвинуть фигуру ниже на ячейку?
    private bool CanFall()
    {
        int[] possibility = new int[statement.GetLength(1)];

        for (int j = 0; j < statement.GetLength(1); j++)
            for (int i = 0; i < statement.GetLength(0); i++)
            {
                if (statement[i, j] != -1)
                {
                    possibility[j] = i;
                    break;
                }
            }

        for (int i = 0; i < statement.GetLength(1); i++)
            if (ud + possibility[i] - 1 < GameController.field.GetLength(0))
                if (GameController.field[ud + possibility[i] - 1, lr + i].block != null)
                    return false;

        return true;
    }

    //private void OnDrawGizmos()
    //{
    //    int[] possibility = new int[statement.GetLength(0)];

    //    for (int i = 0; i < statement.GetLength(0); i++)
    //        for (int j = statement.GetLength(1) - 1; j >= 0; j--)
    //        {
    //            if (statement[i, j] != -1)
    //            {
    //                possibility[i] = j;
    //                break;
    //            }
    //        }

    //    for (int i = 0; i < statement.GetLength(0); i++)
    //        if (lr + possibility[i] + 1 < GameController.field.GetLength(1))
    //            Gizmos.DrawCube(GameController.field[ud + i, lr + possibility[i] + 1].coord, Vector3.one);

    //    possibility = new int[statement.GetLength(0)];

    //    for (int i = 0; i < statement.GetLength(0); i++)
    //        for (int j = 0; j < statement.GetLength(1); j++)
    //        {
    //            if (statement[i, j] != -1)
    //            {
    //                possibility[i] = j;
    //                break;
    //            }
    //        }

    //    for (int i = 0; i < statement.GetLength(0); i++)
    //        if (lr + possibility[i] - 1 >= 0)
    //            Gizmos.DrawCube(GameController.field[ud + i, lr + possibility[i] - 1].coord, Vector3.one);

    //    possibility = new int[statement.GetLength(1)];

    //    for (int j = 0; j < statement.GetLength(1); j++)
    //        for (int i = 0; i < statement.GetLength(0); i++)
    //        {
    //            if (statement[i, j] != -1)
    //            {
    //                possibility[j] = i;
    //                break;
    //            }
    //        }

    //    for (int i = 0; i < statement.GetLength(1); i++)
    //        if (ud + possibility[i] - 1 < GameController.field.GetLength(0) && ud + possibility[i] - 1 > 0)
    //            Gizmos.DrawCube(GameController.field[ud + possibility[i] - 1, lr + i].coord, Vector3.one);
    //}
}
