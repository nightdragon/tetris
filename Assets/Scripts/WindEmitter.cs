﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEmitter: MonoBehaviour
{
    /// <summary>
    /// материал, имитирующий воду
    /// </summary>
    [SerializeField]
    public Material material;
    /// <summary>
    /// ускорение (больше единицы) или замедление (меньше единицы) скорости изменения параметров материала
    /// </summary>
    [Range(0,2)]
    public float timeScale = 0.2f;
    /// <summary>
    /// уменьшение силы искривления материала
    /// </summary>
    [Range(0, 1)]
    public float powerScale = 0.2f;
    /// <summary>
    /// направление ветра
    /// </summary>
    static public Vector2 direction;

    private float power = 1; //сила ветра
    private float newPower; //изменённая сила, вспомогательная для интерполяции
    private Vector2 newDirection; //вспомогательное направление для интерполяции
    private float time; //созранённое время изменения параметров
    private float timer; //время до следующего изменения
    
	private void Start () //инициализируем
    {
        direction = new Vector2(1, 1);
        direction.Normalize();

        material.mainTextureOffset = direction;
        material.SetFloat("_BumpScale", power * powerScale);

        time = Time.time;
        timer = Random.Range(1, 10);
    }

    private void Update () //каждый кадр имитируем ветер и ...
    {
        Wind();

        if(Time.time - time > timer) //... проверяем, не пора ли менять направление и силу
        {
            timer = Random.Range(1, 10);
            GenerateSome();
            time = Time.time;
        }
    }

    private void Wind() //крутим, вертим текстуру и изменяем её рельефность
    {
        power = Mathf.Lerp(power, newPower, Time.deltaTime * timeScale);
        direction = Vector2.Lerp(direction, newDirection, Time.deltaTime * timeScale);
        material.SetFloat("_BumpScale", power * powerScale);
        material.mainTextureOffset += direction * Time.deltaTime * timeScale;
    }

    private void GenerateSome() //изменяем значения как заблагорассудится
    {
        newPower = Random.value;
        newDirection = new Vector2(Random.Range(-1, 1), Random.Range(-1, 1));
        newDirection.Normalize();
        newDirection *= newPower;
    }
}
