﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PewPew : MonoBehaviour
{
    public GameObject missle;
    public float power = 50;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && GameProperties.skills)
        {
            Pew();
        }
        //if (Input.GetMouseButtonDown(1))
        //{
        //    Boom();
        //}
    }

    private void Pew()
    {
        GameController.score *= 0.7f;
        RaycastHit hit = new RaycastHit();
        Ray ray = new Ray();
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.collider.gameObject.tag == "IceCube")
                hit.collider.BroadcastMessage("Split");
        }
    }

    GameObject temp;
    private void Boom()
    {
        if (temp != null)
            temp.BroadcastMessage("Split");

        Ray ray = new Ray();
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        temp = GameController.GetFreeBlock().gameObject;
        temp.GetComponent<IceBlock>().PhysicsOn();
        temp.transform.position = Camera.main.transform.position + Vector3.up;
        temp.transform.rotation = Camera.main.transform.rotation;
        temp.GetComponent<Rigidbody>().velocity = ray.direction * power;
    }
}
