﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Cell
{
    public int id;
    public Vector3 coord;
    public IceBlock block;
}

