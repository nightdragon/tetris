﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class GameProperties
{
    public static GameMode gameMode = GameMode.classic;
    public static bool skills = false;
    public static int width = 10, height = 20;
}

public enum GameMode
{
    classic,
    advanced,
    free
}

