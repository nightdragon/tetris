﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlock : MonoBehaviour
{
    public GameObject destroyedObj; //объект, который заменяет текущий при уничтожении
    public GameObject destroyEffect; //эффект уничтожения
    public float hp = 100; //запас прочности

    private Rigidbody rb; //кешируем обращение к компоненту
    private Collider col;
    private SplinterController sc;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        //объявим сразу же, во избежание лагов
        if (destroyedObj != null)
        {
            destroyedObj = Instantiate(destroyedObj, transform.position, transform.rotation);
            sc = destroyedObj.GetComponent<SplinterController>();
            destroyedObj.SetActive(false);
        }
    }

    private void Start()
    {
        //развернём случайно, чтоб блоки не выглядели одинаково
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x + Random.Range(0, 4) * 90,
        transform.rotation.eulerAngles.y + Random.Range(0, 4) * 90,
        transform.rotation.eulerAngles.z + Random.Range(0, 4) * 90);
    }

    /// <summary>
    /// Разбить блок
    /// </summary>
    public void Split()
    {
        if (destroyedObj != null) //обрабатываем объект, который останется после нас
        {
            destroyedObj.transform.position = this.transform.position;
            destroyedObj.transform.rotation = this.transform.rotation;
            destroyedObj.SetActive(true);

            if (sc != null) //не знаем, что делать, если не знаем, что за фигню заспавнили
            {
                sc.Appear();
                sc.SetVelocity(rb.velocity);
            }
        }
        else if (destroyEffect != null) //обрабатываем эффект, который останется после нас
        {
            GameObject temp = Instantiate(destroyEffect, transform.position, transform.rotation);
            temp.BroadcastMessage("Play");
        }
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Выключить физику блока
    /// </summary>
    public void PhysicsOff()
    {
        col.enabled = false;
        //rb.isKinematic = true;
    }

    /// <summary>
    /// Включить физику блока
    /// </summary>
    public void PhysicsOn()
    {
        col.enabled = true;
        //rb.isKinematic = false;
    }

    //условие разваливания кубика
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.impulse.magnitude > hp)
    //        Split();
    //}
}
