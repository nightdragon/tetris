﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int height, width; //размеры поля
    public List<GameObject> blocks = new List<GameObject>(); //кубики, которыми заполняется pool
    public GameObject endGameMessage; //объект, показывающий окончание игры
    public Text endGameText; //привязанный к endGameMessage текст
    public Text scoreTable; //текущий счёт
    static public List<IceBlock> pool = new List<IceBlock>(); 
    static public Figure current; //текущая падающая фигура
    static public bool gameOver; //вызов функции окончания игры
    static public float score, scoreReady, comboModificator; 

    static public Cell[,] field; //матрица заполненности поля, важна для работы Figure

    private bool isChecked; //флаги для работы автомата
    private bool pause;

    private GameObject figure;

    private void Awake() //создадим пул и игровое поле
    {
        height = GameProperties.height;
        width = GameProperties.width;
        field = new Cell[height, width];

        int poolSize = height * width;
        for (int i = 0; i < poolSize; i++)
        {
            pool.Add(Instantiate(blocks[i % blocks.Count].GetComponent<IceBlock>()));
            pool[pool.Count - 1].gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        figure = new GameObject();

        CalculateCoordinates(); //посчитаем координаты каждой ячейки

        Material mat = new Material(Shader.Find("Standard")); //обозначим вертикальными полосами столбцы
        mat.color = Color.red;
        for (int j = 0; j <= width; j++)
        {
            GameObject temp = new GameObject();
            temp.name = "line renderer " + j;
            LineRenderer line = new LineRenderer();
            line = temp.AddComponent(line.GetType()).GetComponent<LineRenderer>();
            line.material = mat;
            line.startWidth = 0.25f;
            line.endWidth = 0;
            line.positionCount = 2;
            line.SetPosition(0, field[0, 0].coord + Vector3.right * 2 * j + Vector3.left);
            line.SetPosition(1, field[0, 0].coord + Vector3.right * 2 * j + Vector3.left + Vector3.up * 50);
        }
    }

    private void Update ()
    {
        Interpolation();

        if (pause)
        {
            if(current != null)
                current.ready = false;
            return;
        }

        if (gameOver)
            GameOver();

        if (current != null) //управление падающей фигурой
        {
            isChecked = false;
            current.ready = true;
            if (Input.GetKeyDown(KeyCode.A))
                current.MoveLeft();
            else if (Input.GetKeyDown(KeyCode.D))
                current.MoveRight();

            if (Input.GetKeyDown(KeyCode.Q))
                current.TurnLeft();
            else if (Input.GetKeyDown(KeyCode.E))
                current.TurnRight();

            if (Input.GetKeyDown(KeyCode.S))
                current.FallDown();
        }
        else if(!isChecked)
        {
            isChecked = true;
            StartCoroutine(CreateNewFigure()); //создаём новую фигуру, если старая всё
            CheckField(false); //проверим игровое поле на заполненность строки
        }

        scoreReady = Mathf.Lerp(scoreReady, score, Time.deltaTime * 2);
        scoreTable.text = "Score: " + scoreReady.ToString("0"); //отрисовываем очки
    }

    /// <summary>
    /// Вернуть свободный куб из пула
    /// </summary>
    /// <returns></returns>
    static public IceBlock GetFreeBlock()
    {
        foreach (IceBlock b in pool)
            if (!b.isActiveAndEnabled)
            {
                b.gameObject.SetActive(true);
                b.PhysicsOff();
                return b;
            }
        return null;
    }

    /// <summary>
    /// Пауза
    /// </summary>
    public void Pause()
    {
        pause = !pause;
    }

    /// <summary>
    /// Закончить игру и вывести таблицу окончания игры
    /// </summary>
    public void GameOver()
    {
        if (endGameMessage != null)
            endGameMessage.SetActive(true);
        if (endGameText != null)
            endGameText.text = "Your score: " + score;
        pause = true;
    }

    /// <summary>
    /// Перезапустить игру
    /// </summary>
    public void Restart()
    {
        if (endGameMessage != null)
            endGameMessage.SetActive(false);
        score = 0;
        if(current != null)
            current.FallDown();
        pause = true;
        StopAllCoroutines();
        StartCoroutine(SplitAll());
    }

    //разбить все кубы на поле и положить в пул
    IEnumerator SplitAll()
    {

        for (int i = 0; i < height; i++) //двигаем кубы на положенные им места
            for (int j = 0; j < width; j++)
                if (field[i, j].block != null)
                {
                    field[i, j].block.Split();
                    field[i, j].block = null;
                    
                    yield return new WaitForEndOfFrame();
                }

        foreach (IceBlock b in pool)
            b.gameObject.SetActive(false);

        pause = false;
        gameOver = false;
        isChecked = false;
    }

    //двигаем кубы на их законные места
    private void Interpolation()
    {
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                if (field[i, j].block != null)
                {
                    field[i, j].block.transform.position = Vector3.Lerp(field[i, j].block.transform.position, field[i, j].coord, Time.deltaTime * 2);
                    if (!field[i, j].block.gameObject.activeInHierarchy)
                        field[i, j].block = null;
                }
    }

    //проверить поле
    private void CheckField(bool combo)
    {
        for (int i = 0; i < height; i++)
        {
            bool strokeDone = true;
            for (int j = 0; j < width; j++)
            {
                if (field[i, j].block == null)
                    strokeDone = false;
            }
            if (strokeDone)
            {
                if (combo)
                    comboModificator++;
                else
                    comboModificator = 1;
                pause = true;
                DestroyStroke(i);
                return;
            }
            else
                pause = false;
        }

        for (int j = 0; j < width; j++)
        {
            bool columnDone = true;
            for (int i = 0; i < height - 1; i++)
            {
                if (field[i, j].block == null)
                    columnDone = false;
            }
            if (columnDone)
            {
                GameOver();
                return;
            }
        }
    }

    //уничтожить заполненную строчку
    private void DestroyStroke(int strokeNum)
    {
        score += 10 * comboModificator;
        for (int j = 0; j < width; j++)
            if(j == width - 1)
                StartCoroutine(DestroyStrokeProcessing(strokeNum, j, 0.1f * j + 0.5f, true));
            else
                StartCoroutine(DestroyStrokeProcessing(strokeNum, j, 0.1f * j + 0.5f, false));
    }

    //анимация уничтожения строчки
    private IEnumerator DestroyStrokeProcessing(int y, int x, float timeOffset, bool restart)
    {
        yield return new WaitForSeconds(timeOffset);

        field[y, x].block.Split();
        field[y, x].block = null;

        for (int i = y; i < height - 1; i++)
            field[i, x].block = field[i + 1, x].block;

        yield return new WaitForEndOfFrame();

        if (restart)
            CheckField(true);
    }
    
    //создаём новую фигуру
    private IEnumerator CreateNewFigure()
    {
        score++;

        yield return new WaitForSeconds(1f);
        
        float rand = Random.value;

        switch (GameProperties.gameMode)
        {
            case GameMode.classic:
                {
                    if (!gameOver)
                    {
                        if (rand < 0.1f)
                            current = figure.AddComponent(typeof(Figure_1)) as Figure;
                        else if (rand < 0.25f)
                            current = figure.AddComponent(typeof(Figure_2)) as Figure;
                        else if (rand < 0.4f)
                            current = figure.AddComponent(typeof(Figure_3)) as Figure;
                        else if (rand < 0.55f)
                            current = figure.AddComponent(typeof(Figure_4)) as Figure;
                        else if (rand < 0.7f)
                            current = figure.AddComponent(typeof(Figure_5)) as Figure;
                        else if (rand < 0.8f)
                            current = figure.AddComponent(typeof(Figure_6)) as Figure;
                        else
                            current = figure.AddComponent(typeof(Figure_7)) as Figure;
                    }
                    break;
                }
            case GameMode.advanced:
                {
                    if (!gameOver)
                    {
                        if (rand < 0.1f)
                            current = figure.AddComponent(typeof(Figure_1)) as Figure;
                        else if (rand < 0.25f)
                            current = figure.AddComponent(typeof(Figure_2)) as Figure;
                        else if (rand < 0.4f)
                            current = figure.AddComponent(typeof(Figure_3)) as Figure;
                        else if (rand < 0.55f)
                            current = figure.AddComponent(typeof(Figure_4)) as Figure;
                        else if (rand < 0.7f)
                            current = figure.AddComponent(typeof(Figure_5)) as Figure;
                        else if (rand < 0.8f)
                            current = figure.AddComponent(typeof(Figure_6)) as Figure;
                        else if (rand < 0.85f)
                            current = figure.AddComponent(typeof(Figure_7)) as Figure;
                        else if (rand < 0.9f)
                            current = figure.AddComponent(typeof(Figure_8)) as Figure;
                        else if (rand < 0.95f)
                            current = figure.AddComponent(typeof(Figure_9)) as Figure;
                        else
                            current = figure.AddComponent(typeof(Figure_10)) as Figure;
                    }
                    break;
                }
            case GameMode.free:
                {
                    if (!gameOver)
                    {
                        if (rand < 0.1f)
                            current = figure.AddComponent(typeof(Figure_1)) as Figure;
                        else if (rand < 0.25f)
                            current = figure.AddComponent(typeof(Figure_2)) as Figure;
                        else if (rand < 0.4f)
                            current = figure.AddComponent(typeof(Figure_3)) as Figure;
                        else if (rand < 0.55f)
                            current = figure.AddComponent(typeof(Figure_4)) as Figure;
                        else if (rand < 0.7f)
                            current = figure.AddComponent(typeof(Figure_5)) as Figure;
                        else if (rand < 0.8f)
                            current = figure.AddComponent(typeof(Figure_6)) as Figure;
                        else
                            current = figure.AddComponent(typeof(Figure_7)) as Figure;
                    }
                    break;
                }
        }

        yield return new WaitForEndOfFrame();

        if(current != null)
            current.speed = 2;
    }

    //расчитать координаты ячеек поля
    private void CalculateCoordinates()
    {
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                field[i, j].block = null;
                field[i, j].coord = new Vector3((j - width / 2) * 2 + 1, i * 2 + 1, 0) + transform.position;
            }
    }
    
    #region debug_functions
    public void ShowPool()
    {
        StartCoroutine(Showing());
    }

    IEnumerator Showing()
    {
        int n = 0;

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
            {
                pool[n].transform.position = field[i, j].coord;
                pool[n].transform.rotation = transform.rotation;
                pool[n].gameObject.SetActive(true);
                pool[n].PhysicsOff();
                n++;
                yield return new WaitForEndOfFrame();
            }

        foreach (IceBlock b in pool)
            if (b.gameObject.activeInHierarchy)
            {
                b.PhysicsOn();
                yield return new WaitForEndOfFrame();
            }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(new Vector3(0, height, 0) + transform.position, new Vector3(width * 2, height * 2, 2));
    }
    #endregion
}
